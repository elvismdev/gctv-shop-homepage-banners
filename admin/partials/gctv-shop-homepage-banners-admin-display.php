<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://elvismdev.io/
 * @since      1.0.0
 *
 * @package    Gctv_Shop_Homepage_Banners
 * @subpackage Gctv_Shop_Homepage_Banners/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
