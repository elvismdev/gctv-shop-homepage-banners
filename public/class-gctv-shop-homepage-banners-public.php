<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://elvismdev.io/
 * @since      1.0.0
 *
 * @package    Gctv_Shop_Homepage_Banners
 * @subpackage Gctv_Shop_Homepage_Banners/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Gctv_Shop_Homepage_Banners
 * @subpackage Gctv_Shop_Homepage_Banners/public
 * @author     Elvis Morales <elvis@grantcardone.com>
 */
class Gctv_Shop_Homepage_Banners_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gctv_Shop_Homepage_Banners_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gctv_Shop_Homepage_Banners_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gctv-shop-homepage-banners-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gctv_Shop_Homepage_Banners_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gctv_Shop_Homepage_Banners_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gctv-shop-homepage-banners-public.js', array( 'jquery' ), $this->version, false );

	}


	/**
	 * Check for Image and URL to output after Featured Products section and render if true.
	 *
	 */
	public function gctvshophb_after_featured_products(  ) {

		if ( types_render_field( 'after-ft-prods-image', array( 'raw' => true ) ) && types_render_field( 'after-ft-prods-url', array( 'raw' => true ) ) ) {

			$img = types_render_field( 'after-ft-prods-image', array( 'url' => true ) );
			$url = types_render_field( 'after-ft-prods-url', array( 'raw' => true ) );

			$this->gctvshophb_render( $img, $url );

		} 

	}


	/**
	 * Check for Image and URL to output after Webinars Products section and render if true.
	 *
	 */
	public function gctvshophb_after_webinars_products(  ) {

		if ( types_render_field( 'after-webinars-prods-image', array( 'raw' => true ) ) && types_render_field( 'after-webinars-prods-url', array( 'raw' => true ) ) ) {

			$img = types_render_field( 'after-webinars-prods-image', array( 'url' => true ) );
			$url = types_render_field( 'after-webinars-prods-url', array( 'raw' => true ) );

			$this->gctvshophb_render( $img, $url );

		} 

	}


	/**
	 * Check for Image and URL to output after Packages Products section and render if true.
	 *
	 */
	public function gctvshophb_after_packages_products(  ) {

		if ( types_render_field( 'after-packages-prods-image', array( 'raw' => true ) ) && types_render_field( 'after-packages-prods-url', array( 'raw' => true ) ) ) {

			$img = types_render_field( 'after-packages-prods-image', array( 'url' => true ) );
			$url = types_render_field( 'after-packages-prods-url', array( 'raw' => true ) );

			$this->gctvshophb_render( $img, $url );

		} 

	}


	/**
	 * Check for Image and URL to output after eBooks Products section and render if true.
	 *
	 */
	public function gctvshophb_after_ebooks_products(  ) {

		if ( types_render_field( 'after-ebooks-prods-image', array( 'raw' => true ) ) && types_render_field( 'after-ebooks-prods-url', array( 'raw' => true ) ) ) {

			$img = types_render_field( 'after-ebooks-prods-image', array( 'url' => true ) );
			$url = types_render_field( 'after-ebooks-prods-url', array( 'raw' => true ) );

			$this->gctvshophb_render( $img, $url );

		} 

	}


	/**
	 * Check for Image and URL to output after Top Rated Products section and render if true.
	 *
	 */
	public function gctvshophb_after_popular_products(  ) {

		if ( types_render_field( 'after-top-rated-prods-image', array( 'raw' => true ) ) && types_render_field( 'after-top-rated-prods-url', array( 'raw' => true ) ) ) {

			$img = types_render_field( 'after-top-rated-prods-image', array( 'url' => true ) );
			$url = types_render_field( 'after-top-rated-prods-url', array( 'raw' => true ) );

			$this->gctvshophb_render( $img, $url );

		} 

	}


	/**
	 * Check for Image and URL to output after Recent Products section and render if true.
	 *
	 */
	public function gctvshophb_after_recent_products(  ) {

		if ( types_render_field( 'after-recent-prods-image', array( 'raw' => true ) ) && types_render_field( 'after-recent-prods-url', array( 'raw' => true ) ) ) {

			$img = types_render_field( 'after-recent-prods-image', array( 'url' => true ) );
			$url = types_render_field( 'after-recent-prods-url', array( 'raw' => true ) );

			$this->gctvshophb_render( $img, $url );

		} 

	}


	/**
	 * Check for Image and URL to output after On Sale Products section and render if true.
	 *
	 */
	public function gctvshophb_after_on_sale_products(  ) {

		if ( types_render_field( 'after-on-sale-prods-image', array( 'raw' => true ) ) && types_render_field( 'after-on-sale-prods-url', array( 'raw' => true ) ) ) {

			$img = types_render_field( 'after-on-sale-prods-image', array( 'url' => true ) );
			$url = types_render_field( 'after-on-sale-prods-url', array( 'raw' => true ) );

			$this->gctvshophb_render( $img, $url );

		} 

	}


	/**
	 * Render the banner.
	 *
	 */
	public function gctvshophb_render( $img, $url ) {

		include( plugin_dir_path( __FILE__ ) . 'partials/' . $this->plugin_name . '-public-display.php' );

	}

}
