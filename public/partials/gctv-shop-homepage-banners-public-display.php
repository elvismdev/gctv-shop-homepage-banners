<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://elvismdev.io/
 * @since      1.0.0
 *
 * @package    Gctv_Shop_Homepage_Banners
 * @subpackage Gctv_Shop_Homepage_Banners/public/partials
 */
?>

<div class="gctv-shop-homepage-banner">
	<a target="_blank" href="<?php echo $url; ?>">
		<img src="<?php echo $img; ?>">
	</a>
</div>


