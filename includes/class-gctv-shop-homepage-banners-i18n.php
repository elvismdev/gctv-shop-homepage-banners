<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://elvismdev.io/
 * @since      1.0.0
 *
 * @package    Gctv_Shop_Homepage_Banners
 * @subpackage Gctv_Shop_Homepage_Banners/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Gctv_Shop_Homepage_Banners
 * @subpackage Gctv_Shop_Homepage_Banners/includes
 * @author     Elvis Morales <elvis@grantcardone.com>
 */
class Gctv_Shop_Homepage_Banners_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'gctv-shop-homepage-banners',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
