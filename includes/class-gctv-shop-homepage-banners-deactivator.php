<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://elvismdev.io/
 * @since      1.0.0
 *
 * @package    Gctv_Shop_Homepage_Banners
 * @subpackage Gctv_Shop_Homepage_Banners/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Gctv_Shop_Homepage_Banners
 * @subpackage Gctv_Shop_Homepage_Banners/includes
 * @author     Elvis Morales <elvis@grantcardone.com>
 */
class Gctv_Shop_Homepage_Banners_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
